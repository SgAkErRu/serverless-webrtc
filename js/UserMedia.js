class UserMedia
{
	constructor() {
		this.stream = null;
		this.streamConstraints = { "audio": true, "video": true };
		this.captureConstraints =
		{
			video: {
				frameRate: { ideal: 30 }
			},
			audio: true
		}
	}

	checkIsStreamExist()
	{
		return !(this.stream == null);	// -- существует ли стрим -- //
	}

	disablePreviousStream()	// -- выключаем предыдущий стрим, если он существовал -- //
	{
		console.log("Previous stream is exist. Disabling previous stream...");
		this.stream.getTracks().forEach(track => track.stop());
		this.stream = null;
		localVideo.srcObject = null;
		disableBtns(); // -- выключаем кнопки "createOffer и receiveOffer" -- //
	}

	getUserMedia_error(error) // -- в случае, если не удалось захватить потоки юзера -- //
	{
		console.log("> getUserMedia_error():", error);
		if (error.name == "NotFoundError")
		{
			console.log("Webcam or Mic not found.");
			this.streamConstraints = { "audio": true, "video": false }; // -- теперь спрашиваем только микрофон -- //
			this.getUserMedia_click(); // -- вызываем повторно функцию получения мультимедии от юзера -- //
		}
	}

	async getUserMedia_click() // -- получение видео (веб-камера) и аудио (микрофон) потоков -- //
	{
		document.getElementById("modalOkno_userMedia").style.display = "block"; // показываем модальное окно с инструкцией
		btn_getUserMedia.disabled = true;
		btn_getDisplayMedia.disabled = true;
		if ( this.checkIsStreamExist() ) this.disablePreviousStream();
		try {
			this.stream = await navigator.mediaDevices.getUserMedia(this.streamConstraints);
			console.log("getUserMedia success:", this.stream);
			localVideo.srcObject = this.stream; // -- подключаем медиапоток к HTML-элементу <video> (localVideo) -- //

			enableBtns();
		} catch(error) {
			this.getUserMedia_error(error);	// -- в случае ошибки -- //
		}
	}

	async getDisplayMedia_click() // -- захват видео с экрана юзера -- //
	{
		document.getElementById("modalOkno_userMedia").style.display = "block"; // показываем модальное окно с инструкцией
		btn_getUserMedia.disabled = true;
		btn_getDisplayMedia.disabled = true;
		if ( this.checkIsStreamExist() ) this.disablePreviousStream();
		try {
			this.stream = await navigator.mediaDevices.getDisplayMedia(this.captureConstraints);
			console.log("getDisplayMedia success:", this.stream);
			localVideo.srcObject = this.stream; // Подключаем медиапоток к HTML-элементу <video>

			enableBtns();
		} catch(error) {
			console.log("> getDisplayMedia_error():", error);
		}
	}
}