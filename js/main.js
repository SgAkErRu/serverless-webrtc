let UserMediaInstance = new UserMedia();
let PCInstance = new PeerConnection();
document.getElementById("modalOkno_intro").style.display = "block";

// -- элементы из html страницы -- //
// -- кнопки для захвата медиа и создания соединения -- //
const btn_getUserMedia = document.getElementById('btn_getUserMedia');
const btn_getDisplayMedia = document.getElementById('btn_getDisplayMedia');
const btn_createOffer = document.getElementById('btn_createOffer');
const btn_receiveOffer = document.getElementById('btn_receiveOffer');

btn_getUserMedia.addEventListener('click', () => UserMediaInstance.getUserMedia_click());
btn_getDisplayMedia.addEventListener('click', () => UserMediaInstance.getDisplayMedia_click());
btn_createOffer.addEventListener('click', () => PCInstance.createOffer_click());
btn_receiveOffer.addEventListener('click', () => PCInstance.receiveOffer_click());
// -- для модального окна, обрабатываем закрытие окон на кнопку "X" -- //
const btn_close_list = document.getElementsByClassName("close");
for (let btn of btn_close_list) {
	btn.addEventListener('click', () => {
		btn.parentElement.parentElement.style.display = "none";
	});
}


// -- видеоэлементы -- //
const localVideo = document.getElementById('localVideo');
const remoteVideo = document.getElementById('remoteVideo');
// -- секция с чатом и выбором файла для передачи -- //
const afterConnectSection = document.getElementById('afterConnectSection');
// -- поле для ввода SDP и связанные с ним кнопки -- //
const inputSDP = document.getElementById('inputSDP');
const textEdit = document.getElementById('textEdit');
const text_guide = document.getElementById('text_guide');
const btn_send = document.getElementById('btn_send');
btn_send.addEventListener('click', () => PCInstance.send_click());
// -- чат -- //
const chat = document.getElementById('chat');
chat.innerHTML = "";
const messageText = document.getElementById('messageText');
const btn_sendMessage = document.getElementById('btn_sendMessage');
btn_sendMessage.addEventListener('click', () => PCInstance.dc.sendMessage());
// -- для отправки файлов -- //
const btn_sendFile = document.getElementById('btn_sendFile');
const fileInput = document.getElementById('fileInput');
const downloadAnchor = document.getElementById('download');
const receiveProgress = document.getElementById('receiveProgress');
btn_sendFile.addEventListener('click', () => PCInstance.dc.sendFile());
// -- функции, для включения/выключения кнопок "createOffer и receiveOffer" -- //
function enableBtns()
{
	btn_createOffer.disabled = false;
	btn_receiveOffer.disabled = false;
}
function disableBtns()
{
	btn_createOffer.disabled = true;
	btn_receiveOffer.disabled = true;
}

// -- стрелочная функция используется, чтобы this внутри метода класса указывал на класс этого метода, а не на родительское окно контекста -- //
// -- еще один вариант через bind -- //
//btn_getUserMedia.addEventListener('click', UserMediaInstance.getUserMedia_click.bind(UserMediaInstance));
