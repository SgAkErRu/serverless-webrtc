class PeerConnection
{
	constructor() {
		this.pc = null;	// -- peer connection -- //
		this.isOfferSent = false;
		// -- stun/turn сервера -- //
		this.configuration = { "iceServers": [ { "urls": "stun:stun.l.google.com:19302" } ]};
		//this.configuration = null;
		this.offerOptions = {
			offerToReceiveAudio: 1,
			offerToReceiveVideo: 1
		};
		this.dc = new DataChannel();
	}

	async onICECandidate(event)
	{
		console.log("> onICECandidate():", event.candidate);
	}

	onICEStateChange(event)
	{
    	console.log("ICE connection state:", this.pc.iceConnectionState);
		if (this.pc.iceConnectionState == "connected")
		{
			inputSDP.hidden = true;
			afterConnectSection.hidden = false;
		}
    	//console.log("ICE state change event: ", event);
	}

	onICEGatheringStateChange(event) // -- отслеживаем, когда был создан последний ICE-кандидат -- //
	{
		if (this.pc.iceGatheringState == "complete")
		{
			console.log("writing SDP in textEdit...");
			inputSDP.hidden = false; // -- включаем блок с вводом SDP объектов -- //
			textEdit.value = JSON.stringify(PCInstance.pc.localDescription);
		}
	}

	readSDPfromOffer()
	{
		console.log("reading SDP description from textEdit...");
		const desc = new RTCSessionDescription(JSON.parse(textEdit.value));
		return desc;
	}


	async pc_setLocalDescription(desc)
	{
		console.log("setLocalDescription start...");
		// -- устанавливаем приглашение/ответ от нас как описание локальной стороны -- //
		try {
			await this.pc.setLocalDescription(desc);
			console.log("setLocalDescription complete!");
		} catch (error) {
			console.log("Failed to set session description:", error);
		}
	}

	async pc_setRemoteDescription(desc)
	{
		console.log("setRemoteDescription start...");
		// -- устанавливаем приглашение/ответ от нас как описание удаленной стороны -- //
		try {
			await this.pc.setRemoteDescription(desc);
			console.log("setRemoteDescription complete!");
		} catch (error) {
			console.log("Failed to set session description:", error);
		}
	}

	async createOffer_success(desc) // -- приглашение удалось сформировать -- //
	{
		console.log("> createOffer_success()");
		console.log("SDP without candidates was created:", desc);
		this.pc_setLocalDescription(desc);
	}

	async createAnswer_success(desc) // -- ответ на приглашение удалось сформировать -- //
	{
		console.log("> createAnswer_success():", desc);
		this.pc_setLocalDescription(desc);
	}

	async createRTCPeerConnection()
	{
		console.log('RTCPeerConnection configuration:', this.configuration);
		this.pc = new RTCPeerConnection(this.configuration);	// -- создаем RTCPeerConnection -- //
		console.log('Peer connection object is created (pc)', this.pc);
		this.pc.addEventListener('icecandidate', event => this.onICECandidate(event));
		this.pc.addEventListener('iceconnectionstatechange', event => this.onICEStateChange(event));
		this.pc.addEventListener('icegatheringstatechange', event => this.onICEGatheringStateChange(event));
		this.pc.addEventListener('track', event => this.gotRemoteStream(event));

		// -- передаем локальный медиапоток в pc -- //
		const localStream = UserMediaInstance.stream;
		localStream.getTracks().forEach(track => this.pc.addTrack(track, localStream));
		console.log('Added local stream to pc');
	}

	createDataChannel()
	{
		const dataChannelParams = {ordered: true};
		this.dc.message_dc = this.pc.createDataChannel('messaging-channel', dataChannelParams);
		this.dc.file_dc = this.pc.createDataChannel('file-channel', dataChannelParams);
		this.dc.message_dc.binaryType = 'arraybuffer';
		this.dc.file_dc.binaryType = 'arraybuffer';

		this.dc.message_dc.addEventListener('message', (event) => this.dc.receiveMessage(event));
		this.dc.file_dc.addEventListener('message', (event) => this.dc.receiveFile(event));
	}

	async createOffer_click()
	{
		document.getElementById("modalOkno_createOffer").style.display = "block"; // показываем модальное окно с инструкцией

		this.createRTCPeerConnection();


		this.createDataChannel();

		try {	// -- запрашиваем формирования приглашения -- //
			console.log('createOffer start...');
			const offer = await this.pc.createOffer();
			await this.createOffer_success(offer);
		} catch (error) {
			console.log("Failed to create session description (offer):", error);
		}
	}

	async receiveOffer_click()
	{
		document.getElementById("modalOkno_receiveOffer").style.display = "block";

		inputSDP.hidden = false; // -- включаем блок с вводом SDP объектов -- //
		text_guide.innerHTML = "Введите код приглашения:";
		btn_send.value = "Я ввел";
	}

	async receiveOffer()
	{
		console.log("> receiveOffer()");

		this.createRTCPeerConnection();
		this.pc.addEventListener('datachannel', event => this.RemoteDataChannel(event));
		const desc = this.readSDPfromOffer();
		this.pc_setRemoteDescription(desc);

		try {	// -- запрашиваем формирования ответа -- //
			console.log('createAnswer start...');
			const answer = await this.pc.createAnswer();
			await this.createAnswer_success(answer);
		} catch (error) {
			console.log("Failed to create session description (answer):", error);
		}
	}

	RemoteDataChannel(event)
	{
		if (event.channel.label == "messaging-channel")
		{
			this.dc.message_dc = event.channel;
			this.dc.message_dc.binaryType = 'arraybuffer';
			this.dc.message_dc.addEventListener('message', (event) => this.dc.receiveMessage(event));
		}
		else if (event.channel.label == "file-channel")
		{
			this.dc.file_dc = event.channel;
			this.dc.file_dc.binaryType = 'arraybuffer';
			this.dc.file_dc.addEventListener('message', (event) => this.dc.receiveFile(event));
		}

  	}

	async receiveAnswer()
	{
		console.log("> receiveAnswer()");
		const desc = this.readSDPfromOffer();
		this.pc_setRemoteDescription(desc);
	}

	gotRemoteStream(e)
	{
		if (remoteVideo.srcObject !== e.streams[0])
		{
			remoteVideo.srcObject = e.streams[0];
			console.log('pc received remote stream');
		}
	}

	async send_click()
	{
		if (this.pc == null) // -- если соединение еще не создано, а кнопка нажата, то это мы принимаем приглашение -- //
		{
			document.getElementById("modalOkno_createAnswer").style.display = "block";
			this.receiveOffer();
			text_guide.innerHTML = "Скопируйте код ответа:";
			btn_send.hidden = true;
		}
		else	// -- иначе, мы отправляем приглашение -- //
		{
			if (!this.isOfferSent)	// -- если только отправляем -- //
			{
				document.getElementById("modalOkno_receiveAnswer").style.display = "block"; // выводим инструкцию
				this.isOfferSent = true;
				text_guide.innerHTML = "Введите ответ:";
				btn_send.value = "Установить соединение!";
				textEdit.value = "";
			}
			else // -- если уже отправили и нажали на эту кнопку чтобы ввести ответ -- //
			{
				this.receiveAnswer();
				inputSDP.hidden = true;
			}
		}

	}


}