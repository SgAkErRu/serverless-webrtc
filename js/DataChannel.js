class DataChannel
{
	constructor() {
		this.message_dc = null;
		this.file_dc = null;
		this.receiveBuffer = [];
		this.isFileOrFileDesc = 0; // 0 если file desc, 1 если размер файла, 2 если сам файл
		this.receivedSize = 0;
		this.fileSize = 0;
	}

	getTimestamp()
	{
		let timestamp = (new Date).toLocaleString("en-us", {
			hour: "2-digit",
			minute: "2-digit",
			second: "2-digit",
			hour12: false
		});
		return timestamp;
	}

	sendMessage ()
	{
		if (messageText.value) {
			let timestamp = this.getTimestamp();
			chat.innerHTML += "[" + timestamp + "] " + "Я: " + messageText.value + "\n";
			this.message_dc.send(messageText.value);
			messageText.value = "";
		}
	}

	receiveMessage (event)
	{
		let timestamp = this.getTimestamp();
		chat.innerHTML += "[" + timestamp + "] " + "Собеседник: " + event.data + "\n";
	}

	sendFile()
	{
		const file = fileInput.files[0];
		if (file)
		{
			this.file_dc.send(file.name); // отправляем имя+расширение файла
			this.file_dc.send(file.size); // отправляем размер файла
			const chunkSize = 16376;
			let offset = 0;
			let fileReader = new FileReader();
			console.log(file.size);
			fileReader.addEventListener('load', e => {
				this.file_dc.send(e.target.result);
				offset += e.target.result.byteLength;
				if (offset < file.size)
				{
					readSlice(offset);
				}
  			});
			const readSlice = o => {
				const slice = file.slice(offset, o + chunkSize);
				fileReader.readAsArrayBuffer(slice);
			};
			readSlice(0); // отправляем сам файл
			console.log("отправлен файл");
		}
	}

	receiveFile(event)
	{
		if (this.isFileOrFileDesc == 0)
		{
			this.isFileOrFileDesc = 1;
			downloadAnchor.download = event.data;
			receiveProgress.hidden = false;
		}
		else if (this.isFileOrFileDesc == 1)
		{
			this.fileSize = event.data;
			console.log("размер: ", this.fileSize);
			receiveProgress.max = this.fileSize;
			this.isFileOrFileDesc = 2;
		}
		else
		{
			this.receiveBuffer.push(event.data);
			this.receivedSize += event.data.byteLength;
			receiveProgress.value = this.receivedSize;
			if (this.receivedSize == this.fileSize)
			{
				const received = new Blob(this.receiveBuffer);
				this.receiveBuffer = [];
				downloadAnchor.href = URL.createObjectURL(received);
				downloadAnchor.textContent = "Нажмите чтобы скачать файл " + downloadAnchor.download;
				downloadAnchor.style.display = 'block';
				this.isFileOrFileDesc = 0;
				receiveProgress.hidden = true;
			}
		}
	}

}