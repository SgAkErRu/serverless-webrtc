# SERVERLESS WebRTC without signaling server / WebRTC без сигнального сервера

Демо использования WebRTC без сигнального сервера, но не в одной вкладке (например между браузерами на разных компьютерах).

Как пользоваться: 
1. Один браузер создает приглашение (offer) (SDP-объект с описанием соединения и трансляции, объект представляет из себя текстовый код) другому браузеру, и вы отправляете этот код.
Код отправлять любыми средствами надо самим: через IM, дискорд, по почте, в текстовом файле, на бумажке. 
2. После получения приглашения, второй браузер генерирует ответ (answer), который вы отправляете на первый браузер.
3. Готово, соединение должно установиться.

Реализовано на чистом JS + Html, без лишних зависимостей (по крайней мере пока), в ООП стиле (классы).
Функции WebRTC старался использовать самые последние и актуальные. Работу проверял на последних версиях Firefox 75.0 и Chrome 81.0.

Пока реализовано: захват экрана (как всего экрана, так и окна приложения, а в Chrome можно захватывать и звук компьютера), захват вебки и микрофона. Peer-to-peer (p2p) соединение без сигнального сервера, передача видео/звука по p2p.

Планируется: p2p текстовый чат и передача файлов.

Вдохновлялся в основном примерами от Google (но пришлось много перелопатить), а так же просто разной информацией по WebRTC (включая устаревшую).
P.S https://webrtc.github.io/samples/src/content/peerconnection/pc1/

/////////////////////////////////////////

A demo of using WebRTC without signaling server. 

Developed in pure JS + Html, without unnecessary dependencies (at least for now), in OOP style (classes).
I tried to use the latest and most up-to-date WebRTC functions and features. I tested it on the latest versions of Firefox 75.0 and Chrome 81.0.

What done: screen capture (both the entire screen and the application window, and in Chrome you can capture the computer sound), capture the webcam and microphone. Peer-to-peer (p2p) connection without signal server, video/audio transmission over p2p.

Planned: p2p text chat and file transfer.